# YesWeDev - Laravel CMS Nova integration

## Installation

### Nova installation

Install Nova by following [the official installation instructions](https://nova.laravel.com/docs/1.0/installation.html).

### Package installation

```
composer require yeswedev/laravel-cms-nova
```

```
Make resources displayed name configurable
Use `php artisan vendor:publish --tag=laravel-cms-nova` to publish the config file to config/laravel-cms-nova.php
```

Add the Tabs trait to your App\Nova\Resource class.

```
use Arsenaltech\NovaTab\Tabs;

abstract class Resource extends NovaResource
{
    use Tabs;
```

To activate SEO-fields Tab, in your laravel-cms-nova config file enter the key :

```
'seo-fields' => true,
```