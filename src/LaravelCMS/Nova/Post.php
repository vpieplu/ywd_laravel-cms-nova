<?php namespace YesWeDev\LaravelCMS\Nova;

use Illuminate\Http\Request;
use Laravel\Nova\Http\Requests\NovaRequest;

use Laravel\Nova\Fields\BelongsTo;
use Laravel\Nova\Fields\BelongsToMany;
use Laravel\Nova\Fields\Date;
use Laravel\Nova\Fields\HasMany;
use Laravel\Nova\Fields\MorphMany;
use Laravel\Nova\Fields\MorphToMany;
use Laravel\Nova\Fields\Number;
use Laravel\Nova\Fields\Text;
use YesWeDev\Nova\Translatable\Translatable;
use Arsenaltech\NovaTab\NovaTab;

use YesWeDev\LaravelCMS\Post as PostBase;
use Waynestate\Nova\CKEditor;

class Post extends Page
{
    /**
     * The model the resource corresponds to.
     *
     * @var string
     */
    public static $model = PostBase::class;

    /**
     * Get the logical group associated with the resource.
     *
     * @return string
     */
    public static function group()
    {
        return config('laravel-cms-nova.resources.post.group');
    }

    /**
     * Build an "index" query for the given resource.
     *
     * @param  \Laravel\Nova\Http\Requests\NovaRequest $request
     * @param  \Illuminate\Database\Eloquent\Builder $query
     *
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public static function indexQuery(NovaRequest $request, $query)
    {
        return parent::indexQuery($request, $query)->where('type', 'post');
    }

    /**
     * Determine if this resource is available for navigation.
     *
     * @param  \Illuminate\Http\Request $request
     *
     * @return bool
     */
    public static function availableForNavigation(Request $request)
    {
        return config('laravel-cms-nova.post_resource')['post_display'];
    }

    /**
     * Get the displayble label of the resource.
     *
     * @return string
     */
    public static function label()
    {
        return config('laravel-cms-nova.post_names')['post_label'];
    }

    /**
     * Get the displayble singular label of the resource.
     *
     * @return string
     */
    public static function singularLabel()
    {
        return config('laravel-cms-nova.post_names')['post_singularlabel'];
    }

    /**
     * Get the fields displayed by the resource.
     *
     * @param  \Illuminate\Http\Request $request
     *
     * @return array
     */
    public function fields(Request $request)
    {
        $fields = [];

        if (config('laravel-cms-nova.fields.post.title')) {
            if (config('laravel-cms-nova.fields.post.index.title')) {
                $fields[] = Translatable::make('Titre', 'title')
                    ->singleLine();
            } else {
                $fields[] = Translatable::make('Titre', 'title')
                    ->singleLine()->hideFromIndex();
            }
        }
        if (config('laravel-cms-nova.fields.post.description')) {
            if (config('laravel-cms-nova.fields.post.index.description')) {
                if (config('laravel-cms-nova.fields.post.wysiwyg') === 'CKEditor') {
                    $fields[] = CKEditor::make('Contenu', 'description');
                } else {
                    $fields[] = Translatable::make('Contenu', 'description')
                        ->trix();
                }
            } else {
                if (config('laravel-cms-nova.fields.post.wysiwyg') === 'CKEditor') {
                    $fields[] = CKEditor::make('Contenu', 'description')
                        ->hideFromIndex();
                } else {
                    $fields[] = Translatable::make('Contenu', 'description')
                        ->trix()->hideFromIndex();
                }
            }
        }

        if (config('laravel-cms-nova.fields.post.location')) {
            if (config('laravel-cms-nova.fields.post.index.location')) {
                $fields[] = Translatable::make('Lieu', 'location')
                    ->singleLine();
            } else {
                $fields[] = Translatable::make('Lieu', 'location')
                    ->singleLine()->hideFromIndex();
            }
        }

        if (config('laravel-cms-nova.fields.post.url')) {
            if (config('laravel-cms-nova.fields.post.index.url')) {
                $fields[] = Translatable::make('Url', 'url')
                    ->singleLine();
            } else {
                $fields[] = Translatable::make('Url', 'url')
                    ->singleLine()->hideFromIndex();
            }
        }

        if (config('laravel-cms-nova.fields.post.reference')) {
            $fields[] = Text::make('Référence', 'reference')
                ->sortable();
        }

        if (config('laravel-cms-nova.fields.post.publication')) {
            $fields[] = Date::make('Date de publication', 'created_at')
                ->sortable();
        }

        if (config('laravel-cms-nova.fields.post.priority')) {
            $fields[] = Number::make('Priorité', 'priority')
                ->step(0.01)
                ->sortable();
        }

        if (config('laravel-cms-nova.fields.post.attachments') && config('laravel-cms-nova.fichier_resource.fichier_register')) {
            $fields[] = MorphMany::make(Attachment::label(), 'attachments', Attachment::class);
        }

        if (config('laravel-cms-nova.fields.post.blocks') && config('laravel-cms-nova.bloc_resource.bloc_register')) {
            $fields[] = HasMany::make(Block::label(), 'blocks', Block::class);
        }

        if (config('laravel-cms-nova.fields.post.tags') && config('laravel-cms-nova.filtre_resource.filtre_register')) {
            $fields[] = MorphToMany::make(Tag::label(), 'tags', Tag::class);
        }

        if (config('laravel-cms-nova.fields.post.parent') && config('laravel-cms-nova.archive_resource.archive_register')) {
            $fields[] = BelongsTo::make(Archive::singularLabel(), 'parent', Archive::class)->hideFromIndex();
        }

        if (config('laravel-cms-nova.seo-fields')) {
            $fields[] = new NovaTab('Onglet SEO', [
                Text::make('H1', 'seo_h1')->hideFromIndex(),
                Text::make('Méta Titre', 'seo_title')->hideFromIndex(),
                Text::make('Méta Description', 'seo_description')->hideFromIndex(),
            ]);
        }

        return $fields;
    }
}
